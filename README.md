# Start Gate Controller (SGC)

<img src="./doc/img/SGC.jpg">

## Setup your working environment

Go on [https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install) to install

## Launch SGC

```bash
cd /src
cargo run
```
