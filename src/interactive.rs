use crate::parser::Cli;
use std::{io, error::Error};
use shellwords;
use clap::Parser;

pub fn read_from_stdin () -> String {
    let mut input = String::new(); // Take user input (to be parsed as clap args)
    io::stdin().read_line(&mut input).expect("Error reading input.");
    format!("sgc {input}")
}

pub fn parse_string_as_command (input: String) -> Result<Cli, Box<dyn Error>> {
    let words = shellwords::split(&input)?;
    // println!("Debug: {:?}", words);
    Ok(Cli::try_parse_from(words)?)
}
