//! # hypervisor
//!
//! `hypervisor` is a library for interacting with hypervisors and libvirt.

use std::error::Error;
use std::collections::HashMap;
use std::io;

use clap::error::Result;
use subprocess::Exec;
use uuid::Uuid;
use virt::domain::Domain;
use virt::connect::{Connect, ConnectAuth, ConnectCredential};
use virt::sys;

use crate::utils::render_template;

/// Struct to represent an hypervisor
pub struct Hypervisor {
    /// The hypervisor connector from libvirt
    connector: Connect,
    /// A HashMap that holds all the domains of the hypervisor
    domains: HashMap<String, Domain>,
}

impl Hypervisor {
    /// Function to create a new hypervisor
    ///
    /// # Arguments
    /// * `url_hpir` - A string slice that holds the url of the hypervisor
    /// 
    /// # Example
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// ```
    pub fn new(url_hpir: &str) -> Result<Hypervisor, Box<dyn Error>> {
        let connector = Connect::open(&url_hpir)?;
        let flags = sys::VIR_CONNECT_LIST_DOMAINS_ACTIVE |sys::VIR_CONNECT_LIST_DOMAINS_INACTIVE;
        let all_domains: Vec<Domain> = connector.list_all_domains(flags)?;
        let mut domains = HashMap::new();
        
        for domain in all_domains{
            let name: String = domain.get_name().expect("fetching vm name");
            domains.insert(name, domain);
        }

        Ok(Hypervisor { connector, domains })
    }


    /// Function to create a new hypervisor with authentication
    /// 
    /// # Arguments
    /// * `url_hpir` - A string slice that holds the url of the hypervisor
    pub fn new_auth(url_hpir: String) -> Result<Hypervisor, Box<dyn Error>> {

        fn callback(creds: &mut Vec<ConnectCredential>) {
            for cred in creds {
                let mut input = String::new();
    
                println!("{}:", cred.prompt);
                match cred.typed as u32 {
                    sys::VIR_CRED_AUTHNAME => {
                        io::stdin().read_line(&mut input).expect("");
                        cred.result = Some(String::from(input.trim()));
                    }
                    sys::VIR_CRED_PASSPHRASE => {
                        io::stdin().read_line(&mut input).expect("");
                        cred.result = Some(String::from(input.trim()));
                    }
                    _ => {
                        panic!("Should not be here...");
                    }
                }
            }
        }

        let mut auth = ConnectAuth::new(
            vec![sys::VIR_CRED_AUTHNAME, sys::VIR_CRED_PASSPHRASE],
            callback,
        );
        let connector = Connect::open_auth(&url_hpir, &mut auth, 0)?;
        let flags = sys::VIR_CONNECT_LIST_DOMAINS_ACTIVE;
        let domains_obj = connector.list_all_domains(flags)?;
        let mut domains = HashMap::new();
        
        for domain in domains_obj {
            let name = domain.get_name().expect("getting name from domain");
            domains.insert(name, domain);
        }

        Ok(Hypervisor { connector, domains })
    }

    /// Function to list all active domains of the hypervisor
    /// 
    /// # Example
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.list_active_domains();
    /// ```
    pub fn list_active_domains(&self) -> Result<Vec<Domain>, Box<dyn Error>> {
        let flags = sys::VIR_CONNECT_LIST_DOMAINS_ACTIVE;
        Ok(self.connector.list_all_domains(flags)?)
    }

    /// Function to list all inactive domains of the hypervisor
    /// 
    /// # Example
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.list_inactive_domains();
    /// ```
    pub fn list_inactive_domains(&self) -> Result<Vec<Domain>, Box<dyn Error>> {
        let flags = sys::VIR_CONNECT_LIST_DOMAINS_INACTIVE;
        Ok(self.connector.list_all_domains(flags)?)
    }

    /// Function to list all domains of the hypervisor and print them
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.list_domains();
    /// ```
    pub fn list_domains(self){
        for (name,domain) in self.domains{
            let active = if domain.is_active().unwrap() {"Yes"} else {"No"};
            println!("Name : {}\nActive : {}\nInfo : {:?}\n----------",name, active, domain.get_info().unwrap());
        }
    }

    /// Function to rename a domain
    /// 
    /// # Arguments
    /// * `old_name` - A string slice that holds the old name of the domain
    /// * `new_name` - A string slice that holds the new name of the domain
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.rename_domain("vm1", "vm2");
    /// ```
    pub fn rename_domain(self,old_name:&str,new_name:&str) -> Result<String, String> {
        let Some(selected_domain) = self.domains.get(old_name) else {
            return Err(format!("The vm you want to rename couldn't be found"));
        };
        if selected_domain.is_active().unwrap_or(true){
            return Err(format!("Cannot rename active virtual machine"));
        }
        match selected_domain.rename(new_name, 0){
            Ok(_) => Ok(format!("Succesfully renamed {} to {}!",old_name,new_name)),
            Err(err) => Err(format!("Couldn't rename {} due to {:?}",old_name,err)),
        }
    }

    /// Function to start a domain
    /// 
    /// # Arguments
    /// * `domain_name` - A string slice that holds the name of the domain
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.start_domain("vm1");
    /// ```
    pub fn start_domain(self,domain_name:&str) -> Result<String, String> {
        let Some(selected_domain) = self.domains.get(domain_name) else {
            return Err(format!("The vm you want to start couldn't be found"));
        };
        if selected_domain.is_active().unwrap_or(true){
            return Err(format!("Virtual machine is already active"));
        }
        match selected_domain.create(){
            Ok(_) => Ok(format!("Succesfully started {}!",domain_name)),
            Err(err) => Err(format!("Couldn't start {} due to {:?}",domain_name,err)),
        }
    }

    /// Function to stop a domain
    /// 
    /// # Arguments
    /// * `domain_name` - A string slice that holds the name of the domain
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.stop_domain("vm1");
    /// ```
    pub fn stop_domain(self,domain_name:&str) -> Result<String, String>{
        let Some(selected_domain) = self.domains.get(domain_name) else {
            return Err(format!("The vm you want to stop couldn't be found"));
        };
        if !selected_domain.is_active().unwrap_or(true){
            return Err(format!("Virtual machine is already inactive"));
        }
        match selected_domain.shutdown(){
            Ok(_) => {
                if selected_domain.is_active().unwrap_or(false){
                    return match selected_domain.destroy() {
                        Ok(_) => Ok(format!("Virtual machine is resisting normal shutdown, switching to destroying")),
                        Err(e) => Err(format!("Couldn't stop nor destroy domain : {:?}", e))
                    };
                }
                return Ok(format!("Succesfully stopped {}!",domain_name));
            },
            Err(err) => Err(format!("Couldn't stop {} due to {:?}",domain_name,err)),
        }
    }

    /// Function to clone a domain
    /// 
    /// # Arguments
    /// * `domain_name` - A string slice that holds the name of the domain
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.clone_domain("vm1");
    /// ```
    pub fn clone_domain(self,domain_name:&str) -> Result<String, String>{
        let Some(selected_domain) = self.domains.get(domain_name) else {
            return Err(format!("The vm you want to stop couldn't be found"));
        };

        let Ok(selected_xml) = selected_domain.get_xml_desc(0) else {
            return Err(format!("Failed to fetch XML"));
        };
        let Ok(selected_uuid) = selected_domain.get_uuid_string() else {
            return Err(format!("Failed to fetch UUID"));
        };

        let mut new_name = domain_name.to_owned();
        new_name.push_str("_clone");
        let new_uuid = Uuid::new_v4().to_string();
        
        let mut xml_clone = selected_xml.replace(domain_name,new_name.as_str());
        xml_clone = xml_clone.replace(&selected_uuid, &new_uuid);
        match Domain::define_xml(&self.connector, &xml_clone){
            Ok(_) => Ok(format!("Clone successful, new virtual machine is named {new_name}")),
            Err(err) => Err(format!("Clone failure because of {err}")),
        }
    }

    /// Function to get the hypervisor domains
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// use virt::domain::Domain;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// let domains = hypervisor.get_domains();
    /// ```
    pub fn get_domains(&self) -> &HashMap<String, Domain> {
        &self.domains
    }

    /// Function to get all informations about the hypervisor
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.get_all_informations();
    /// ```
    pub fn get_all_informations(self) -> Result<(), Box<dyn Error>> {
        let ifaces = self.connector.list_interfaces()?;
        let networks = self.connector.list_networks()?;
        let storages = self.connector.list_storage_pools()?;

        print!("----------- Hypervisor informations -----------\n");
        print!("Hypervisor: \n{:?}\n", self.connector.get_hostname());
        print!("Interface: \n{:?}\n", ifaces);
        print!("Networks: \n{:?}\n", networks);
        print!("Storages: \n{:?}\n", storages);
        print!("{} domain(s): \n", self.domains.len());
        self.list_domains();
        Ok(())
    }

    /// Function to create a new disk
    /// # Arguments
    ///
    /// * `name` - A string slice that holds the name of the disk
    /// * `data` - A HashMap that holds the data of the disk
    ///
    fn create_disk(data: &HashMap<String, String>) -> Result<String, Box<dyn Error>> {
        let filepath = data.get("filepath").ok_or("Missing filepath")?;
        let disk_size = data.get("disk_size").ok_or("Missing disk_size")?;

        let str_command = format!(
            "qemu-img create -f qcow2 {}.qcow2 {}G",
            filepath, disk_size
        );

        let _dir_checksum = {
            Exec::shell(str_command)
        }.capture()?.stdout_str();
        Ok(format!("Creation of disk {}.qcow2", data.get("filepath").ok_or("Missing filepath")?))
    }   

    /// Function to create a new virtual machine
    /// 
    /// # Arguments
    /// * `name` - A string slice that holds the name of the virtual machine
    /// * `data` - A HashMap that holds the data of the virtual machine
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.create_vm("vm1", "10","1024","1","/home/xavier/Downloads/disk_name","/home/user/Downloads/ubuntu-22.04.3-desktop-amd64.iso",data);
    /// ```
    pub fn create_vm(&mut self, name: &String, disk_size: &String, memory: &String, vcpu: &String, filepath: &String, iso: &Option<String>) -> Result<String, Box<dyn Error>>{
        let mut data:HashMap<String,String> = HashMap::new();
        data.insert("name".to_string(), name.to_owned());
        data.insert("disk_size".to_string(), disk_size.to_owned());
        data.insert("memory".to_string(), memory.to_owned());
        data.insert("vcpu".to_string(), vcpu.to_owned());
        data.insert("filepath".to_string(), filepath.to_owned());
        if iso.is_some(){
            data.insert("iso".to_string(), iso.clone().unwrap());
        }
        Hypervisor::create_disk(&data)?;

        let xml = render_template("vm_template.xml", "vm_template.xml", data)?;

        let domain = Domain::create_xml(&self.connector, &xml, 0)?;

        self.domains.insert(domain.get_name().unwrap(), domain);
        Ok(format!("VM created successfully !"))
    }

    /// Function to reboot a virtual machine
    /// 
    /// # Arguments
    /// * `name` - A string slice that holds the name of the virtual machine
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.reboot_vm("vm1");
    /// ```
    pub fn reboot_vm(&mut self, name: &str) -> Result<(), Box<dyn Error>>{
        let domain = self.domains.get(name).ok_or("Missing name")?;
        Ok(domain.reboot(0)?)
    }


    /// Function to delete a virtual machine
    /// 
    /// # Arguments
    /// * `domain_name` - A string slice that holds the name of the virtual machine
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor = Hypervisor::new("qemu+ssh://localhost/system");
    /// hypervisor.delete_domain("vm1");
    /// ```
    pub fn delete_domain(&self, domain_name: &str) -> Result<String, String>{
        let Some(selected_domain) = self.domains.get(domain_name) else {
            return Err(format!("The vm you want to stop couldn't be found"));
        };

        if selected_domain.is_active().unwrap_or(true) {
            return Err(format!("Virtual machine is running, cannot delete"));
        }

        let delete_flags = sys::VIR_DOMAIN_UNDEFINE_MANAGED_SAVE | sys::VIR_DOMAIN_UNDEFINE_SNAPSHOTS_METADATA | sys::VIR_DOMAIN_UNDEFINE_CHECKPOINTS_METADATA | sys::VIR_DOMAIN_UNDEFINE_NVRAM;
        match selected_domain.undefine_flags(delete_flags){
            Ok(_) => Ok(format!("Succesfully deleted {}!",domain_name)),
            Err(err) => Err(format!("Couldn't start {} due to {:?}",domain_name,err))
        }
    }


    /// Function to migrate a virtual machine
    /// 
    /// # Arguments
    /// * `vm_name` - A string slice that holds the name of the virtual machine
    /// * `url` - A string slice that holds the url of the hypervisor
    /// 
    /// # Example
    /// 
    /// ```
    /// use Hypervisor;
    /// 
    /// let hypervisor1 = Hypervisor::new("qemu+ssh://my_ip_source/system");
    /// hypervisor1.migrate("vm1", "qemu+ssh://my_ip_dest/system");
    /// ```
    pub fn migrate(&self, vm_name: &str , url: &str) -> Result<String, String>{
        if let Some(domain)= self.domains.get(vm_name) {
            match domain.migrate(&self.connector, 1, url, 0) {
                Err(e) => return Err(format!("Error migration {:?}", e)),
                Ok(_) => return Ok(format!("Migration successful"))
            }
        }
        return Err(format!("Error VM not found"));

    }
}
