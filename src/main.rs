//! # SGC CLI
//!
//! `SGC` is a CLI to manage virtual machines on a remote hypervisor.

use clap::Parser;
use parser::*;
use std::{env, io::{self, Write}};

mod hypervisor;
mod parser;
mod state;
mod utils;
mod interactive;

/// Launch SGC CLI
fn main() {
    // let default_hyperv_url = "qemu+ssh://localhost/system";
    // let iso_image = "/home/xavier/Downloads/ubuntu-22.04.3-desktop-amd64.iso";
    // let disk_image = "/home/xavier/Downloads/disk.qcow2";
    let home_dir = env::var("HOME").unwrap_or(String::from("."));
    let storage_folder = format!("{home_dir}/.sgc");
    let state_path = format!("{storage_folder}/state.json");

    let mut global_state = state::AppState::load_from(&state_path);
    if global_state.urls.len() < 1 {
        global_state.add_hypervisor(String::from("local"), String::from("qemu+ssh://localhost/system"));
        global_state.select_hypervisor("local");
    }

    let cli = Cli::parse();

    match &cli.command {
        // You can check for the existence of subcommands, and if found use their
        // matches just as you would the top level cmd
        Some(commands) => handle_commands(commands, &mut global_state, &storage_folder, &state_path),
        None => {
            println!("Welcome to Stargate Controler Interactive mode.");
            print_help();
            loop {
                if let Some(hypv) = global_state.get_selected_hypervisor() {
                    print!("\n{hypv} > ");
                } else {
                    print!("\n> ");
                }
                let _ = io::stdout().flush();
                let read = interactive::read_from_stdin();
                let temp_cli = interactive::parse_string_as_command(read);
                if let Ok(user_cmd) = temp_cli {
                    handle_commands(&user_cmd.command.unwrap(), &mut global_state, &storage_folder, &state_path);
                    let _ = global_state.save_to(&storage_folder, &state_path);
                } else {
                    println!("Invalid command !");
                    print_help();
                }
                println!("");
            }
        }
    }

    global_state.save_to(&storage_folder, &state_path).expect("Failed to save the state.json file");
}


fn print_help () {
    println!("Available commands :");
    println!("- hypervisor : list | add <name> <url> | delete <name> | info | select <name> | unselect");
    println!("- vm : list | stop <name> | start <name> | clone <name> | rename <old_name> <new_name> | migrate <name> <hypervisor url>");
    println!("- vm : create <name> <disk in Gb> <memory in Mb> <vcpu> <qcow2 file path> [iso name] | delete <name>");
    println!("- image : list | add <name> <path> | delete <name>");
    println!("Others commands : `help` to display this message, `exit` to quit gracefully");
}

fn handle_commands (cmd: &Commands, global_state: &mut state::AppState, storage_folder: &str, state_path: &str) {
    match cmd {
        Commands::Vm(args) => match global_state.open_selected() {
            Some(mut hyperv) => match &args.commands {
                VmCommands::Create{vm_name, disk_size, memory,vcpu,filepath,iso} => match iso {
                    Some(iso_name) => match global_state.get_iso(iso_name) {
                        Some(iso_path) => match hyperv.create_vm(vm_name,disk_size, memory, vcpu, filepath, &Some(iso_path)) {
                            Ok(s) => println!("{s}"),
                            Err(e) => eprintln!("Failed to create domain : {:?}", e)
                        },
                        None => eprintln!("Iso image not found, list using `image list` and add using `image add <name> <path>` !")
                    },
                    None => match hyperv.create_vm(vm_name,disk_size, memory, vcpu, filepath, iso) {
                        Ok(s) => println!("{s}"),
                        Err(e) => eprintln!("Failed to create domain : {:?}", e)
                    }
                },
                VmCommands::Delete{vm_name} => match hyperv.delete_domain(vm_name) {
                    Ok(s) => println!("{s}"),
                    Err(e) => eprintln!("Failed to delete domain : {:?}", e)
                },
                VmCommands::Stop{vm_name} => match hyperv.stop_domain(vm_name) {
                    Ok(s) => println!("{s}"),
                    Err(e) => eprintln!("Failed to stop domain : {:?}", e)
                },
                VmCommands::Start{vm_name} => match hyperv.start_domain(vm_name) {
                    Ok(s) => println!("{s}"),
                    Err(e) => eprintln!("Failed to start domain : {:?}", e)
                },
                VmCommands::Clone{vm_name} => match hyperv.clone_domain(vm_name) {
                    Ok(s) => println!("{s}"),
                    Err(e) => eprintln!("Failed to clone domain : {:?}", e)
                },
                VmCommands::List => hyperv.list_domains(),
                VmCommands::Rename{old_name, new_name} => match hyperv.rename_domain(old_name, new_name) {
                    Ok(s) => println!("{s}"),
                    Err(e) => eprintln!("Failed to rename domain : {:?}", e)
                },
                VmCommands::Migrate{vm_name, url} => match hyperv.migrate(vm_name, url) {
                    Ok(s) => println!("{s}"),
                    Err(e) => eprintln!("Failed to migrate domain : {:?}", e)
                },
            },
            None => eprintln!("Either you haven't selected an hypervisor, or can't connect to them")
        },
        Commands::Hypervisor(args) => match &args.commands{
            HypervisorCommands::Add{name, url} => global_state.add_hypervisor(name.to_string(), url.to_string()),
            HypervisorCommands::Delete{name} => global_state.remove_hypervisor(name),
            HypervisorCommands::List => global_state.list_hypervisors(),
            HypervisorCommands::Info => match global_state.open_selected() {
                Some(hyperv) => match hyperv.get_all_informations() {
                    Ok(_) => (),
                    Err(e) => eprintln!("Failed to get information : {:?}", e)
                },
                None => eprintln!("Either you haven't selected an hypervisor, or can't connect to them")
            },
            HypervisorCommands::Select {name} => match global_state.select_hypervisor(name) {
                true => println!("Hypervisor selected !"),
                false => eprintln!("Hypervisor cannot be selected !")
            },
            HypervisorCommands::Unselect => {
                global_state.reset_selected_hypervisor();
                println!("Unselected hypervisor !");
            }
        },
        Commands::Image(args) => match &args.commands{
            ImageCommands::Add{name, path} => global_state.add_iso(name.to_string(), path.to_string()),
            ImageCommands::Delete{name} => global_state.remove_iso(name),
            ImageCommands::List => global_state.list_isos(),
        },
        Commands::Exit => {
            match global_state.save_to(storage_folder, state_path) {
                Ok(_) => std::process::exit(0),
                Err(e) => eprintln!("Failed to save the state.json file, aborting exit (Use Ctrl+C to force stopping)\n{:?}", e)
            }
        }
    }
}
