use clap::{Parser, Subcommand, Args};

/// Start Gate Controller (SGC) : a command line tool allowing you to manage your virtual machines
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Option<Commands>,
}

/// Main commands
#[derive(Subcommand)]
pub enum Commands {
    /// Manage virtual machines
    Vm(VmArgs),
    /// Manage hypervisors
    Hypervisor(HypervisorArgs),
    /// Manage virtual machines images
    Image(ImageArgs),
    Exit
}

/// Virtual machine subcommand structure
#[derive(Args)]
pub struct VmArgs{
    #[command(subcommand)]
    pub commands: VmCommands,
}

/// Virtual machine subcommand list
#[derive(Subcommand)]
pub enum VmCommands{
    /// Create a new virtual machine
    Create{vm_name:String, disk_size:String,memory:String,vcpu:String,filepath:String,iso:Option<String>},
    /// Delete a virtual machine
    Delete{vm_name:String},
    /// Poweroff a running virtual machine
    Stop{vm_name:String},
    /// Start a virtual machine
    Start{vm_name:String},
    /// Clone a virtual machine
    Clone{vm_name:String},
    /// List all virtual machines
    List,
    /// Rename a virtual machine
    Rename{old_name:String,new_name:String},
    /// Migrate a virtual machine
    Migrate{vm_name:String,url:String}
}

/// Hypervisor subcommand structure
#[derive(Args)]
pub struct HypervisorArgs{
    #[command(subcommand)]
    pub commands: HypervisorCommands,
}

/// Hypervisor subcommand list
#[derive(Subcommand)]
pub enum HypervisorCommands{
    /// Add an hypervisor
    Add{name: String, url: String},
    /// Delete an hypervisor
    Delete{name: String},
    /// List all hypervisors
    List,
    /// Get data on the given hypervisor
    Info,
    /// Select an Hypervisor for interacting with it
    Select{name: String},
    /// Unselect hypervisor to stop connecting to it
    Unselect,
}

/// Images subcommand structure
#[derive(Args)]
pub struct ImageArgs{
    #[command(subcommand)]
    pub commands: ImageCommands,
}

/// Images subcommand list
#[derive(Subcommand)]
pub enum ImageCommands{
    /// Add an image file (.iso)
    Add{name: String, path: String},
    /// Delete an image
    Delete{name: String},
    /// List all images
    List,
}