#![feature(proc_macro_hygiene, decl_macro)]

use std::{vec::Vec, collections::HashMap};
use std::env;

use rocket::{fs::FileServer, get, launch, routes, data};

mod templates;

use crate::state::AppState;
use self::templates::{TupleItems};

pub fn creat_state() -> AppState {
    let mut home_dir = env::var("HOME").unwrap_or(String::from("."));
    let storage_folder = format!("{home_dir}/.sgc");
    let state_path = format!("{storage_folder}/state.json");
    AppState::load_from(&state_path)
}

#[rocket::main]
pub async fn rocket() -> Result<(), rocket::Error> {
    let _rocket = rocket::build()
        .mount("/", routes![index])
        .mount("/public", FileServer::from("public"))
        // .mount("/hypervisor", routes![hypervisor_page])
        .launch()
        .await?;
    Ok(())
}

#[get("/")]
fn index() -> templates::Index {
    let mut global_state = creat_state();
    let mut data: Vec<TupleItems> = Vec::new();

    for (name, path) in global_state.urls {
            data.push(TupleItems{
                key: name,
                data: path
            })
        }

    templates::Index {
        title: String::from("Start Gate Controller"),
        hypervisors: data,
    }
}

// #[get("/<hpvr_name>")]
// fn hypervisor_page(hpvr_name: String) -> templates::Hypervisor {
//     let mut global_state = creat_state();
//     let mut data: Vec<GenericDomain> = Vec::new();
//     let mut hypervisor = global_state.open_url(&hpvr_name).unwrap();

//     for (name, domain) in hypervisor.get_domains(){
//         let mut d_data: HashMap<String, String> = HashMap::new();
//         d_data.insert("is_active".to_string(), domain.is_active().unwrap().to_string());
//         data.push(GenericDomain { 
//             name: name.clone(), 
//             data: d_data 
//         })
//     }

//     templates::Hypervisor {
//         title: String::from("Start Gate Controller"),
//         domains: data
//     }
// }