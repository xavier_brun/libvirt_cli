use serde::{Deserialize, Serialize};
use std::{collections::HashMap, error::Error, fs};

use crate::hypervisor::Hypervisor;

#[derive(Serialize, Deserialize, Debug)]
pub struct AppState {
    pub iso_images: HashMap<String, String>,
    pub urls: HashMap<String, String>,
    pub selected_hypervisor: Option<String>
}

impl AppState {

    pub fn empty () -> AppState {
        AppState {
            iso_images: HashMap::new(),
            urls: HashMap::new(),
            selected_hypervisor: None,
        }
    }

    pub fn load_from (path: &str) -> AppState {
        let raw_json = match fs::read_to_string(path) {
            Ok(json) => json,
            Err(_) => String::from("{}"),
        };
        match serde_json::from_str(&raw_json) {
            Ok(state) => state,
            Err(_) => AppState::empty(),
        }
    }

    pub fn save_to (&self, folder_path: &str, file_path: &str) -> Result<(), Box<dyn Error>> {
        let json = serde_json::to_string(&self)?;
        fs::create_dir_all(folder_path)?;
        fs::write(file_path, json)?;
        Ok(())
    }

    // pub fn open_url (&mut self, name: &str) -> Option<Hypervisor> {
    //     let value = self.get_hypervisor(name)?;
    //     Hypervisor::new(value.as_str()).ok()
    // }

    pub fn open_selected (&mut self) -> Option<Hypervisor> {
        let selected = self.selected_hypervisor.clone()?;
        let value = self.get_hypervisor(&selected)?;
        Hypervisor::new(value.as_str()).ok()
    }



    pub fn add_iso (&mut self, name: String, iso_path: String) {
        self.iso_images.insert(name, iso_path);
    }

    pub fn remove_iso (&mut self, name: &str) {
        self.iso_images.remove(name);
    }

    pub fn get_iso (&mut self, name: &str) -> Option<String> {
        let value = self.iso_images.get(name)?.clone();
        Some(value)
    }

    pub fn list_isos (&self) {
        println!("        name         |         chemin");
        println!("---------------------|--------------------");
        for (name, path) in &self.iso_images {
            println!("{:^20} | {}", name, path);
        }
    }




    pub fn add_hypervisor (&mut self, name: String, url: String) {
        self.urls.insert(name, url);
    }

    pub fn remove_hypervisor (&mut self, name: &str) {
        self.urls.remove(name);
    }

    pub fn get_hypervisor (&mut self, name: &str) -> Option<String> {
        let value = self.urls.get(name)?.clone();
        Some(value)
    }

    pub fn list_hypervisors (&self) {
        println!("        name         |         url");
        println!("---------------------|--------------------");
        for (name, url) in &self.urls {
            if self.selected_hypervisor.as_ref().is_some_and(|x| x == name) {
                println!("* {:^18} | {}", name, url);
            } else {
                println!("{:^20} | {}", name, url);
            }
        }
    }

    pub fn select_hypervisor (&mut self, name: &str) -> bool {
        if self.urls.get(name).is_some() {
            self.selected_hypervisor = Some(name.to_string());
            return true;
        }
        false
    }

    pub fn get_selected_hypervisor (&self) -> Option<String> {
        self.selected_hypervisor.clone()
    }

    pub fn reset_selected_hypervisor (&mut self) {
        self.selected_hypervisor = None;
    }
}
