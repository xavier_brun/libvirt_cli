//! # utils
//!
//! `utils` is a library of useful functions.

// use std::env;
use std::fs;
use std::error::Error;
use std::collections::HashMap;

use minijinja::{Environment, context};

/// Return template content
/// 
/// # Arguments
/// * `tpl_name` - Template name
/// * `file_source` - Template file source
/// * `data` - Template data
/// 
pub fn render_template(tpl_name: &str, file_source: &str, data: HashMap<String, String>) ->  Result<String, Box<dyn Error>> {

    let contents = fs::read_to_string(file_source)?;

    let mut env = Environment::new();
    env.add_template(tpl_name, &contents)?;
    let tmpl = env.get_template(tpl_name)?;
    let xml = tmpl.render(context!(
        name => data.get("name").unwrap(),
        memory => data.get("memory").unwrap(),
        vcpu => data.get("vcpu").unwrap(),
        filepath => data.get("filepath").unwrap(),
        iso => data.get("iso"),
    ))?;
    Ok(xml)
}
