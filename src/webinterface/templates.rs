use askama::Template;
use virt::domain::Domain;

use std::vec::Vec;
use std::collections::HashMap;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index {
    pub title: String,
    pub hypervisors: Vec<TupleItems>,
}

// #[derive(Template)]
// #[template(path = "hypervisor.html")]
// pub struct Hypervisor {
//     pub title: String,
//     pub domains: Vec<GenericDomain>,
// }

pub struct TupleItems {
    pub key: String,
    pub data: String
}

// pub struct GenericDomain {
//     pub name: String,
//     pub data: HashMap<String, String>
// }

impl std::fmt::Display for TupleItems {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(fmt, "key {}\ndata: {}", self.key, self.data)
    }
}

// impl std::fmt::Display for GenericDomain {
//     fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
//         write!(fmt, "key {}\ndata: {:?}", self.name, self.data)
//     }
// }
